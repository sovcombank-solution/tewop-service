package com.anvacon.tewop;

public class Names {

    public final static String SECURITY_SCHEME = "security_openid";

    public final static String DATABASE_SCHEMA = "public";

    public final static String EMPLOYEE_TABLE_NAME = "tewop_employee";

    public final static String TASK_TYPE_TABLE_NAME = "tewop_task_type";

    public final static String BASE_LOCATION_TABLE_NAME = "tewop_base_location";

    public final static String LOCATION_TABLE_NAME = "tewop_location";

    public final static String TASK_TABLE_NAME = "tewop_task";

    public final static String ASSIGNMENT_TABLE_NAME = "tewop_assignment";
}
