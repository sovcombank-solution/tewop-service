package com.anvacon.tewop.specification;

import com.anvacon.tewop.entity.Task;
import com.anvacon.tewop.entity.Task_;
import com.anvacon.tewop.entity.enumeration.Priority;
import lombok.val;
import org.springframework.data.jpa.domain.Specification;

public class TaskSpecification {

    public static Specification<Task> all() {
        return (root, cq, cb) -> cb.conjunction();
    }

    public static Specification<Task> hasId(Long id) {
        if (id == null) {
            return null;
        }
        return (root, cq, cb) -> cb.equal(root.get(Task_.id), id);
    }

    public static Specification<Task> assigned(Boolean assigned) {
        if (assigned == null) {
            return null;
        }
        val attribute = Task_.assigned;
        return (root, cq, cb) -> assigned ? cb.isTrue(root.get(attribute)) : cb.isFalse(root.get(attribute));
    }

    public static Specification<Task> hasCurrentPriority(Priority currentPriority) {
        if (currentPriority == null) {
            return null;
        }
        return (root, cq, cb) -> cb.equal(root.get(Task_.currentPriority), currentPriority);
    }
}
