package com.anvacon.tewop.specification;


import com.anvacon.tewop.entity.BaseLocation;
import com.anvacon.tewop.entity.Employee;
import com.anvacon.tewop.entity.Employee_;
import com.anvacon.tewop.entity.enumeration.Degree;
import lombok.val;
import org.springframework.data.jpa.domain.Specification;

import java.util.UUID;

import static org.springframework.util.StringUtils.hasText;

public class EmployeeSpecification {

    public static Specification<Employee> all() {
        return (root, cq, cb) -> cb.conjunction();
    }

    public static Specification<Employee> hasId(UUID id) {
        if (id == null) {
            return null;
        }
        return (root, cq, cb) -> cb.equal(root.get(Employee_.id), id);
    }

    public static Specification<Employee> hasPersonnelNumber(String personnelNumber) {
        if (personnelNumber == null) {
            return null;
        }
        return (root, cq, cb) -> cb.equal(root.get(Employee_.personnelNumber), personnelNumber);
    }

    public static Specification<Employee> firstNameContainsIgnoreCase(String firstName) {
        if (!hasText(firstName)) {
            return null;
        }
        return (root, cq, cb) -> cb.like(
            cb.upper(root.get(Employee_.firstName)),
            cb.upper(cb.literal("%" + firstName + "%"))
        );
    }

    public static Specification<Employee> lastNameContainsIgnoreCase(String lastName) {
        if (!hasText(lastName)) {
            return null;
        }
        return (root, cq, cb) -> cb.like(
            cb.upper(root.get(Employee_.lastName)),
            cb.upper(cb.literal("%" + lastName + "%"))
        );
    }

    public static Specification<Employee> patronymicContainsIgnoreCase(String patronymic) {
        if (!hasText(patronymic)) {
            return null;
        }
        return (root, cq, cb) -> cb.like(
            cb.upper(root.get(Employee_.patronymic)),
            cb.upper(cb.literal("%" + patronymic + "%"))
        );
    }

    public static Specification<Employee> hasEmail(String email) {
        if (email == null) {
            return null;
        }
        return (root, cq, cb) -> cb.equal(root.get(Employee_.email), email);
    }

    public static Specification<Employee> hasDegree(Degree degree) {
        if (degree == null) {
            return null;
        }
        return (root, cq, cb) -> cb.equal(root.get(Employee_.degree), degree);
    }

    public static Specification<Employee> hasBaseLocation(BaseLocation baseLocation) {
        if (baseLocation == null) {
            return null;
        }
        return (root, cq, cb) -> cb.equal(root.get(Employee_.baseLocation), baseLocation);
    }

    public static Specification<Employee> fioContainsIgnoreCase(String fio) {
        if (!hasText(fio)) {
            return null;
        }
        val first = firstNameContainsIgnoreCase(fio);
        val last = lastNameContainsIgnoreCase(fio);
        val patronymic = patronymicContainsIgnoreCase(fio);
        if (first == null || last == null || patronymic == null) {
            return null;
        }
        return Specification.where(first.or(last).or(patronymic));
    }

    public static Specification<Employee> employee(Boolean employee) {
        if (employee == null) {
            return null;
        }
        val attribute = Employee_.employee;
        return (root, cq, cb) -> employee ? cb.isTrue(root.get(attribute)) : cb.isFalse(root.get(attribute));
    }

    public static Specification<Employee> available(Boolean available) {
        if (available == null) {
            return null;
        }
        val attribute = Employee_.available;
        return (root, cq, cb) -> available ? cb.isTrue(root.get(attribute)) : cb.isFalse(root.get(attribute));
    }

    public static Specification<Employee> agent(Boolean agent) {
        if (agent == null) {
            return null;
        }
        val attribute = Employee_.agent;
        return (root, cq, cb) -> agent ? cb.isTrue(root.get(attribute)) : cb.isFalse(root.get(attribute));
    }
}
