package com.anvacon.tewop.configuration;

import com.anvacon.tewop.converter.JwtRolesGrantedAuthoritiesConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.annotation.web.configurers.CorsConfigurer;
import org.springframework.security.config.annotation.web.configurers.CsrfConfigurer;
import org.springframework.security.config.annotation.web.configurers.FormLoginConfigurer;
import org.springframework.security.config.annotation.web.configurers.HttpBasicConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

@Configuration
public class SecurityConfiguration {

    private final String rolesClaimName;

    public SecurityConfiguration(@Value("${app.roles-claim-name}") String rolesClaimName) {
        this.rolesClaimName = rolesClaimName;
    }

    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
        return (web) -> web.ignoring().requestMatchers(
            "/static/**", "/v3/api-docs/**", "/swagger-ui.html",
            "/swagger-ui/**", "/openapi/openapi.yml",
            "/api/v3/api-docs/**", "/api/swagger-ui/**",
            "/api/swagger-ui.html", "/api/openapi/openapi.yml"
        );
    }

    @Bean
    public SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
        http
            .cors(CorsConfigurer::disable)
            .csrf(CsrfConfigurer::disable)
            .httpBasic(HttpBasicConfigurer::disable)
            .formLogin(FormLoginConfigurer::disable)
            .oauth2ResourceServer(oauth2 -> oauth2.jwt(Customizer.withDefaults()))
            .sessionManagement(sm -> sm.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
            .authorizeHttpRequests((authorize) -> authorize
                .requestMatchers(AntPathRequestMatcher.antMatcher("/actuator/**")).permitAll()
                .requestMatchers(AntPathRequestMatcher.antMatcher("/api/actuator/**")).permitAll()
                .requestMatchers("/error/**").permitAll()
                .requestMatchers(GET, "/api/v1/references/**").permitAll()
                .requestMatchers(GET, "/api/v1/task-types/**").permitAll()
                .requestMatchers(GET, "/api/v1/authenticated-user/oidc-user-info").authenticated()
                .requestMatchers("/api/v1/base-locations/**").hasRole("MANAGER")
                .requestMatchers("/api/v1/employees/**").hasRole("MANAGER")
                .requestMatchers("/api/v1/locations/**").hasRole("MANAGER")
                .requestMatchers("/api/v1/tasks/**").hasRole("MANAGER")
                .requestMatchers("api/v1/planning/**").hasRole("MANAGER")
                .requestMatchers("api/v1/assignments/**").hasAnyRole("FIELD_WORKER", "MANAGER")
                .anyRequest().denyAll())
            .exceptionHandling(exceptionHandling ->
                exceptionHandling.authenticationEntryPoint((request, response, authException) -> {
                    response.addHeader(HttpHeaders.WWW_AUTHENTICATE, "Bearer");
                    response.sendError(UNAUTHORIZED.value(), UNAUTHORIZED.getReasonPhrase());
                }));
        return http.build();
    }

    @Bean
    public Converter<Jwt, AbstractAuthenticationToken> customJwtAuthenticationConverter() {
        JwtAuthenticationConverter converter = new JwtAuthenticationConverter();
        converter.setJwtGrantedAuthoritiesConverter(new JwtRolesGrantedAuthoritiesConverter(rolesClaimName));
        return converter;
    }
}
