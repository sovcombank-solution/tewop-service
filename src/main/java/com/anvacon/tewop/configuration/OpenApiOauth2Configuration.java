package com.anvacon.tewop.configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityScheme;

import static com.anvacon.tewop.Names.SECURITY_SCHEME;
import static io.swagger.v3.oas.annotations.enums.SecuritySchemeIn.HEADER;

@OpenAPIDefinition(info = @Info(title = "Traveling employee work planner API",
    description = "Traveling employee work planner", version = "${springdoc.app-version}"))
@SecurityScheme(name = SECURITY_SCHEME, type = SecuritySchemeType.OPENIDCONNECT,
    openIdConnectUrl = "${springdoc.oidc.openid-configuration-url}",
    scheme = "bearer", bearerFormat = "jwt", in = HEADER)
public class OpenApiOauth2Configuration {
}
