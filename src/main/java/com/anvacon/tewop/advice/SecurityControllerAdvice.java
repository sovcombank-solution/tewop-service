package com.anvacon.tewop.advice;

import com.anvacon.tewop.dto.sso.OpenIDConnectUser;
import com.anvacon.tewop.dto.sso.OpenIDConnectUserProfile;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtClaimNames;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@ControllerAdvice
public class SecurityControllerAdvice {

    public static final String OIDC_USER_ID = "openIDConnectUserId";
    public static final String OIDC_USER_INFO = "openIDConnectUserInfo";

    private final String principalClaimName = JwtClaimNames.SUB;

    @ModelAttribute(name = OIDC_USER_ID, binding = false)
    public Optional<UUID> openIDConnectUserId(Authentication authentication) {
        if (authentication != null && authentication.isAuthenticated() &&
            authentication instanceof JwtAuthenticationToken token) {
            final Optional<String> sub = Optional
                .ofNullable(token.getToken().getClaimAsString(this.principalClaimName));
            return sub.map(UUID::fromString);
        }
        return Optional.empty();
    }

    @ModelAttribute(name = OIDC_USER_INFO, binding = false)
    public Optional<OpenIDConnectUser> openIDConnectUserInfo(Authentication authentication) {
        if (authentication != null && authentication.isAuthenticated() &&
            authentication instanceof JwtAuthenticationToken jwtAuthenticationToken) {
            return Optional.of(extractOpenIDConnectUser(jwtAuthenticationToken));
        }
        return Optional.empty();
    }

    private OpenIDConnectUser extractOpenIDConnectUser(JwtAuthenticationToken token) {
        final Jwt jwt = token.getToken();
        final String sub = jwt.getClaimAsString(this.principalClaimName);
        final UUID id = sub != null ? UUID.fromString(sub) : null;
        final List<String> authorities = token.getAuthorities().stream()
            .map(GrantedAuthority::getAuthority)
            .toList();
        OpenIDConnectUserProfile profile = OpenIDConnectUserProfile.builder()
            .uniquePrincipalName(jwt.getClaimAsString("preferred_username"))
            .email(jwt.getClaimAsString("email"))
            .emailVerified(jwt.getClaimAsBoolean("email_verified"))
            .familyName(jwt.getClaimAsString("family_name"))
            .givenName(jwt.getClaimAsString("given_name"))
            .patronymic(jwt.getClaimAsString("patronymic"))
            .picture(jwt.getClaimAsString("picture"))
            .build();
        return new OpenIDConnectUser(id, profile, authorities);
    }
}
