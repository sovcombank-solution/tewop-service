package com.anvacon.tewop.repository;

import com.anvacon.tewop.entity.Location;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LocationRepository extends JpaRepository<Location, Long> {

    List<Location> findAllByTakenIntoAccountWhenPlanningIsTrueOrderByExternalId();
}
