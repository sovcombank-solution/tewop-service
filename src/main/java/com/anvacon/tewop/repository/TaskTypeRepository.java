package com.anvacon.tewop.repository;

import com.anvacon.tewop.entity.TaskType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskTypeRepository extends JpaRepository<TaskType, Integer> {
}
