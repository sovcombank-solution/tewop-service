package com.anvacon.tewop.repository;

import com.anvacon.tewop.entity.Assignment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface AssignmentRepository extends JpaRepository<Assignment, Long> {

    List<Assignment> findAllByPlannedDateOrderByEmployeePersonnelNumberAscSerialNumberAsc(
        LocalDate plannedDate);

    List<Assignment> findAllByPlannedDateAndEmployeeIdOrderBySerialNumber(
        LocalDate plannedDate, UUID employeeId);
}
