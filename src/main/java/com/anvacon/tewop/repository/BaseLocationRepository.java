package com.anvacon.tewop.repository;

import com.anvacon.tewop.entity.BaseLocation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BaseLocationRepository extends JpaRepository<BaseLocation, Integer> {
}
