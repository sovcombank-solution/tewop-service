package com.anvacon.tewop.provider;

import com.anvacon.tewop.dto.geo.Coordinates;

public interface GeospatialProvider {

    double distance(Coordinates c1, Coordinates c2);
}
