package com.anvacon.tewop.provider;

import com.anvacon.tewop.dto.geo.Coordinates;
import org.geotools.referencing.GeodeticCalculator;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.springframework.stereotype.Component;

@Component
public class GeospatialProviderImpl implements GeospatialProvider {

    /**
     * @param c1 координаты начальной точки
     * @param c2 координаты конечной точки
     * @return дистанция между точками в километрах
     */
    @Override
    public double distance(Coordinates c1, Coordinates c2) {
        GeodeticCalculator calculator = new GeodeticCalculator(DefaultGeographicCRS.WGS84);
        calculator.setStartingGeographicPoint(c1.getLongitude().doubleValue(), c1.getLatitude().doubleValue());
        calculator.setDestinationGeographicPoint(c2.getLongitude().doubleValue(), c2.getLatitude().doubleValue());
        return calculator.getOrthodromicDistance() / 1000;
    }
}
