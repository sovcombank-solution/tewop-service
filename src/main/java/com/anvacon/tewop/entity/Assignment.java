package com.anvacon.tewop.entity;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalTime;

import static com.anvacon.tewop.Names.ASSIGNMENT_TABLE_NAME;
import static com.anvacon.tewop.Names.DATABASE_SCHEMA;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = ASSIGNMENT_TABLE_NAME, schema = DATABASE_SCHEMA)
public class Assignment {

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "planned_date", columnDefinition = "DATE", nullable = false)
    private LocalDate plannedDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "employee_id", nullable = false)
    private Employee employee;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "task_id", nullable = false)
    private Task task;

    @Column(name = "start_time", columnDefinition = "TIME", nullable = false)
    private LocalTime startTime;

    /**
     * Порядковый номер задачи назначенной сотруднику в указанный день
     */
    @Column(name = "serial_number", nullable = false)
    private int serialNumber;

    /**
     * Если порядковый номер 1, сотрудник выезжал из базовой локации
     * в противном случае с локации предыдущей задачи
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "starting_base_location_id")
    private BaseLocation startingBaseLocation;

    /**
     * Локация предыдущей задачи, если порядковый номер 1, то null
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "starting_location_id")
    private Location startingLocation;

    /**
     * Длина маршрута в метрах
     */
    @Column(name = "distance", nullable = false)
    private int distance;

    /**
     * Продолжительность маршрута в минутах
     */
    @Column(name = "route_duration", nullable = false)
    private int routeDuration;

    @Column(name = "cumulative_duration", nullable = false)
    private int cumulativeDuration;

    @Column(name = "canceled", nullable = false)
    private boolean canceled;

    @Column(name = "failed", nullable = false)
    private boolean failed;

    @Column(name = "completed", nullable = false)
    private boolean completed;
}
