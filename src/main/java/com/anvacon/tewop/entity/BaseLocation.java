package com.anvacon.tewop.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.BatchSize;

import java.math.BigDecimal;
import java.util.Set;

import static com.anvacon.tewop.Names.BASE_LOCATION_TABLE_NAME;
import static com.anvacon.tewop.Names.DATABASE_SCHEMA;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = BASE_LOCATION_TABLE_NAME, schema = DATABASE_SCHEMA)
public class BaseLocation {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "longitude")
    private BigDecimal longitude;

    @Column(name = "latitude")
    private BigDecimal latitude;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "baseLocation", fetch = FetchType.LAZY)
    @BatchSize(size = 5)
    private Set<Employee> employees;

    @OneToMany(mappedBy = "startingBaseLocation", fetch = FetchType.LAZY)
    @BatchSize(size = 25)
    private Set<Assignment> assignments;
}
