package com.anvacon.tewop.entity;

import com.anvacon.tewop.entity.enumeration.Degree;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.BatchSize;

import java.util.Set;
import java.util.UUID;

import static com.anvacon.tewop.Names.DATABASE_SCHEMA;
import static com.anvacon.tewop.Names.EMPLOYEE_TABLE_NAME;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = EMPLOYEE_TABLE_NAME, schema = DATABASE_SCHEMA)
public class Employee {

    @Id
    @Column(name = "id", nullable = false)
    private UUID id;

    @Column(name = "personnel_number", nullable = false)
    private String personnelNumber;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "patronymic")
    private String patronymic;

    @Column(name = "email")
    private String email;

    @Column(name = "avatar")
    private String avatar;

    @Column(name = "degree")
    private Degree degree;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "base_location_id")
    private BaseLocation baseLocation;

    @Column(name = "employee", nullable = false)
    private boolean employee;

    @Column(name = "available", nullable = false)
    private boolean available;

    @Column(name = "agent", nullable = false)
    private boolean agent;

    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    @BatchSize(size = 25)
    private Set<Assignment> assignments;
}
