package com.anvacon.tewop.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.BatchSize;

import java.math.BigDecimal;
import java.util.Set;

import static com.anvacon.tewop.Names.DATABASE_SCHEMA;
import static com.anvacon.tewop.Names.LOCATION_TABLE_NAME;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = LOCATION_TABLE_NAME, schema = DATABASE_SCHEMA)
public class Location {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    /**
     * Номер точки из исходных данных
     */
    @Column(name = "external_id", nullable = false)
    private Long externalId;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "longitude")
    private BigDecimal longitude;

    @Column(name = "latitude")
    private BigDecimal latitude;

    /**
     * Новая точка,
     * true - новая (подключена вчера)
     * false - не новая (подключена давно)
     */
    @Column(name = "new_place", nullable = false)
    private boolean newPlace;

    /**
     * Карты и материалы доставлены
     */
    @Column(name = "cards_and_docs_delivered", nullable = false)
    private boolean cardsAndDocumentsDelivered;

    /**
     * Количество дней после выдачи последней карты
     */
    @Column(name = "days_after_issuance", nullable = false)
    private Integer daysAfterIssuance;

    /**
     * Количество одобренных заявок
     */
    @Column(name = "approved_applications", nullable = false)
    private Integer approvedApplications;

    /**
     * Количество выданных карт
     */
    @Column(name = "cards_issued", nullable = false)
    private Integer cardsIssued;

    /**
     * Учитывать локацию при планировании
     */
    @Column(name = "taken_into_account_when_planning", nullable = false)
    private boolean takenIntoAccountWhenPlanning;

    @OneToMany(mappedBy = "location", fetch = FetchType.LAZY)
    @BatchSize(size = 25)
    private Set<Task> tasks;

    @OneToMany(mappedBy = "startingLocation", fetch = FetchType.LAZY)
    @BatchSize(size = 25)
    private Set<Assignment> assignments;
}
