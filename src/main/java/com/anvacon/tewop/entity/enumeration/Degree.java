package com.anvacon.tewop.entity.enumeration;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public enum Degree {

    JUNIOR(1, "Младший специалист"),

    MIDDLE(2, "Специалист"),

    SENIOR(3, "Старший специалист"),

    LEAD(4, "Менеджер");

    private static final Degree[] VALUES;

    public static List<Degree> findAll() {
        return Arrays.stream(VALUES).toList();
    }

    static {
        VALUES = values();
    }

    private final int id;

    private final String preferredName;

    Degree(int id, String preferredName) {
        this.id = id;
        this.preferredName = preferredName;
    }

    public int getId() {
        return this.id;
    }

    public String getPreferredName() {
        return this.preferredName;
    }

    public Degree.DegreeDto getDto() {
        return new Degree.DegreeDto(this.id, this.name(), this.preferredName);
    }

    public static Optional<Degree> findById(Integer degreeId) {
        if (degreeId == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(resolve(degreeId));
    }

    public static Degree valueOf(Integer degreeId) {
        if (degreeId == null) {
            return null;
        }
        Degree degree = resolve(degreeId);
        if (degree == null) {
            throw new IllegalArgumentException("No matching constant for [" + degreeId + "]");
        }
        return degree;
    }

    public static Degree resolve(int degreeId) {
        for (Degree degree : VALUES) {
            if (degree.id == degreeId) {
                return degree;
            }
        }
        return null;
    }

    public record DegreeDto(int id, String name, String preferredName) {
    }
}
