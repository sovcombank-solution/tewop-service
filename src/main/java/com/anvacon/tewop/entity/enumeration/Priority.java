package com.anvacon.tewop.entity.enumeration;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public enum Priority {

    HIGH(1, "Высокий"),

    MEDIUM(2, "Средний"),

    LOW(3, "Низкий");

    private static final Priority[] VALUES;

    public static List<Priority> findAll() {
        return Arrays.stream(VALUES).toList();
    }

    static {
        VALUES = values();
    }

    private final int id;

    private final String levelName;

    Priority(int id, String levelName) {
        this.id = id;
        this.levelName = levelName;
    }

    public int getId() {
        return this.id;
    }

    public String getLevelName() {
        return levelName;
    }

    public Priority.PriorityDto getDto() {
        return new Priority.PriorityDto(this.id, this.name(), this.levelName);
    }

    public static Optional<Priority> findById(Integer priorityId) {
        if (priorityId == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(resolve(priorityId));
    }

    public static Priority valueOf(Integer priorityId) {
        if (priorityId == null) {
            return null;
        }
        Priority priority = resolve(priorityId);
        if (priority == null) {
            throw new IllegalArgumentException("No matching constant for [" + priorityId + "]");
        }
        return priority;
    }

    public static Priority resolve(int priorityId) {
        for (Priority priority : VALUES) {
            if (priority.id == priorityId) {
                return priority;
            }
        }
        return null;
    }

    public record PriorityDto(int id, String name, String levelName) {
    }
}
