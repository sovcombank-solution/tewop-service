package com.anvacon.tewop.entity;

import com.anvacon.tewop.entity.enumeration.Priority;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.BatchSize;

import java.util.Set;

import static com.anvacon.tewop.Names.DATABASE_SCHEMA;
import static com.anvacon.tewop.Names.TASK_TABLE_NAME;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = TASK_TABLE_NAME, schema = DATABASE_SCHEMA)
public class Task {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "location_id", nullable = false)
    private Location location;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "task_type_id", nullable = false)
    private TaskType taskType;

    @Column(name = "assigned", nullable = false)
    private boolean assigned;

    @Column(name = "canceled", nullable = false)
    private boolean canceled;

    @Column(name = "overdue", nullable = false)
    private boolean overdue;

    @Column(name = "current_priority", nullable = false)
    private Priority currentPriority;

    @Column(name = "completed", nullable = false)
    private boolean completed;

    @OneToMany(mappedBy = "task", fetch = FetchType.LAZY)
    @BatchSize(size = 25)
    private Set<Assignment> assignments;
}
