package com.anvacon.tewop.entity;

import com.anvacon.tewop.entity.enumeration.Degree;
import com.anvacon.tewop.entity.enumeration.Priority;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.BatchSize;

import java.util.Set;

import static com.anvacon.tewop.Names.DATABASE_SCHEMA;
import static com.anvacon.tewop.Names.TASK_TYPE_TABLE_NAME;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = TASK_TYPE_TABLE_NAME, schema = DATABASE_SCHEMA)
public class TaskType {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "priority", nullable = false)
    private Priority priority;

    /**
     * Нормативное время выполнения задачи в минутах
     */
    @Column(name = "duration", nullable = false)
    private Integer duration;

    @Column(name = "minimum_degree")
    private Degree minimumDegree;

    @OneToMany(mappedBy = "taskType", fetch = FetchType.LAZY)
    @BatchSize(size = 5)
    private Set<Task> tasks;
}
