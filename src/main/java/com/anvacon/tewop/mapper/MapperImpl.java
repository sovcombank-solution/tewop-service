package com.anvacon.tewop.mapper;

import com.anvacon.tewop.dto.*;
import com.anvacon.tewop.entity.*;
import com.anvacon.tewop.entity.enumeration.Degree;
import com.anvacon.tewop.entity.enumeration.Priority;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.function.Function;

@Component
public class MapperImpl implements Mapper {

    @Override
    public TaskTypeDto mapToTaskTypeDto(TaskType source) {
        if (source == null) {
            return null;
        }
        return TaskTypeDto.builder()
            .id(source.getId())
            .name(source.getName())
            .priority(source.getPriority().getDto())
            .duration(source.getDuration())
            .minimumDegree(source.getMinimumDegree().getDto())
            .build();
    }

    @Override
    public List<TaskTypeDto> mapToTaskTypeDtos(List<TaskType> source) {
        return source.stream().map(this::mapToTaskTypeDto).toList();
    }

    @Override
    public BaseLocation mapToBaseLocation(BaseLocationDto source) {
        if (source == null) {
            return null;
        }
        return BaseLocation.builder()
            .id(source.getId())
            .address(source.getAddress())
            .longitude(source.getLongitude())
            .latitude(source.getLatitude())
            .description(source.getDescription())
            .build();
    }

    @Override
    public BaseLocationDto mapToBaseLocationDto(BaseLocation source) {
        if (source == null) {
            return null;
        }
        return BaseLocationDto.builder()
            .id(source.getId())
            .address(source.getAddress())
            .longitude(source.getLongitude())
            .latitude(source.getLatitude())
            .description(source.getDescription())
            .build();
    }

    @Override
    public List<BaseLocationDto> mapToBaseLocationDtos(List<BaseLocation> source) {
        return source.stream().map(this::mapToBaseLocationDto).toList();
    }

    @Override
    public Employee mapToEmployee(EmployeeSaveDto source, Function<Integer, BaseLocation> idToBaseLocationProxy) {
        if (source == null) {
            return null;
        }
        return Employee.builder()
            .id(source.getId())
            .personnelNumber(source.getPersonnelNumber())
            .firstName(source.getFirstName())
            .lastName(source.getLastName())
            .patronymic(source.getPatronymic())
            .email(source.getEmail())
            .avatar(source.getAvatar())
            .degree(Degree.valueOf(source.getDegreeId()))
            .baseLocation(idToBaseLocationProxy.apply(source.getBaseLocationId()))
            .employee(source.isEmployee())
            .available(source.isAvailable())
            .agent(source.isAgent())
            .build();
    }

    @Override
    public EmployeeDto mapToEmployeeDto(Employee source) {
        if (source == null) {
            return null;
        }
        return EmployeeDto.builder()
            .id(source.getId())
            .personnelNumber(source.getPersonnelNumber())
            .firstName(source.getFirstName())
            .lastName(source.getLastName())
            .patronymic(source.getPatronymic())
            .email(source.getEmail())
            .avatar(source.getAvatar())
            .degree(source.getDegree().getDto())
            .baseLocation(mapToBaseLocationDto(source.getBaseLocation()))
            .employee(source.isEmployee())
            .available(source.isAvailable())
            .agent(source.isAgent())
            .build();
    }

    @Override
    public Location mapToLocation(LocationDto source) {
        if (source == null) {
            return null;
        }
        return Location.builder()
            .id(source.getId())
            .externalId(source.getExternalId())
            .address(source.getAddress())
            .longitude(source.getLongitude())
            .latitude(source.getLatitude())
            .newPlace(source.isNewPlace())
            .cardsAndDocumentsDelivered(source.isCardsAndDocumentsDelivered())
            .daysAfterIssuance(source.getDaysAfterIssuance())
            .approvedApplications(source.getApprovedApplications())
            .cardsIssued(source.getCardsIssued())
            .takenIntoAccountWhenPlanning(source.isTakenIntoAccountWhenPlanning())
            .build();
    }

    @Override
    public LocationDto mapToLocationDto(Location source) {
        if (source == null) {
            return null;
        }
        return LocationDto.builder()
            .id(source.getId())
            .externalId(source.getExternalId())
            .address(source.getAddress())
            .longitude(source.getLongitude())
            .latitude(source.getLatitude())
            .newPlace(source.isNewPlace())
            .cardsAndDocumentsDelivered(source.isCardsAndDocumentsDelivered())
            .daysAfterIssuance(source.getDaysAfterIssuance())
            .approvedApplications(source.getApprovedApplications())
            .cardsIssued(source.getCardsIssued())
            .takenIntoAccountWhenPlanning(source.isTakenIntoAccountWhenPlanning())
            .build();
    }

    @Override
    public List<LocationDto> mapToLocationDtos(List<Location> source) {
        return source.stream().map(this::mapToLocationDto).toList();
    }

    @Override
    public Task mapToTask(TaskSaveDto source,
                          Function<Long, Location> idToLocationProxy,
                          Function<Integer, TaskType> idToTaskTypeProxy) {
        if (source == null) {
            return null;
        }
        return Task.builder()
            .id(source.getId())
            .location(idToLocationProxy.apply(source.getLocationId()))
            .taskType(idToTaskTypeProxy.apply(source.getTaskTypeId()))
            .canceled(source.isCanceled())
            .overdue(source.isOverdue())
            .currentPriority(Priority.valueOf(source.getCurrentPriorityId()))
            .completed(source.isCompleted())
            .build();
    }

    @Override
    public TaskDto mapToTaskDto(Task source) {
        if (source == null) {
            return null;
        }
        return TaskDto.builder()
            .id(source.getId())
            .location(mapToLocationDto(source.getLocation()))
            .taskType(mapToTaskTypeDto(source.getTaskType()))
            .canceled(source.isCanceled())
            .overdue(source.isOverdue())
            .currentPriority(source.getCurrentPriority().getDto())
            .completed(source.isCompleted())
            .build();
    }

    @Override
    public List<TaskDto> mapToTaskDtos(List<Task> source) {
        return source.stream().map(this::mapToTaskDto).toList();
    }

    @Override
    public Assignment mapToAssignment(AssignmentSaveDto source,
                                      Function<UUID, Employee> idToEmployeeProxy,
                                      Function<Long, Task> idToTaskProxy,
                                      Function<Integer, BaseLocation> idToBaseLocationProxy,
                                      Function<Long, Location> idToLocationProxy) {
        if (source == null) {
            return null;
        }
        return Assignment.builder()
            .id(source.getId())
            .plannedDate(source.getPlannedDate())
            .employee(idToEmployeeProxy.apply(source.getEmployeeId()))
            .task(idToTaskProxy.apply(source.getTaskId()))
            .startTime(source.getStartTime())
            .serialNumber(source.getSerialNumber())
            .startingBaseLocation(idToBaseLocationProxy.apply(source.getStartingBaseLocationId()))
            .startingLocation(idToLocationProxy.apply(source.getStartingLocationId()))
            .distance(source.getDistance())
            .routeDuration(source.getRouteDuration())
            .cumulativeDuration(source.getCumulativeDuration())
            .canceled(source.isCanceled())
            .failed(source.isFailed())
            .completed(source.isCompleted())
            .build();
    }

    @Override
    public AssignmentDto mapToAssignmentDto(Assignment source) {
        if (source == null) {
            return null;
        }
        return AssignmentDto.builder()
            .id(source.getId())
            .plannedDate(source.getPlannedDate())
            .employee(mapToEmployeeDto(source.getEmployee()))
            .task(mapToTaskDto(source.getTask()))
            .startTime(source.getStartTime())
            .serialNumber(source.getSerialNumber())
            .startingBaseLocation(mapToBaseLocationDto(source.getStartingBaseLocation()))
            .startingLocation(mapToLocationDto(source.getStartingLocation()))
            .distance(source.getDistance())
            .routeDuration(source.getRouteDuration())
            .cumulativeDuration(source.getCumulativeDuration())
            .canceled(source.isCanceled())
            .failed(source.isFailed())
            .completed(source.isCompleted())
            .build();
    }

    @Override
    public List<AssignmentDto> mapToAssignmentDtos(List<Assignment> source) {
        return source.stream().map(this::mapToAssignmentDto).toList();
    }
}
