package com.anvacon.tewop.mapper;

import com.anvacon.tewop.dto.*;
import com.anvacon.tewop.entity.*;

import java.util.List;
import java.util.UUID;
import java.util.function.Function;

public interface Mapper {

    TaskTypeDto mapToTaskTypeDto(TaskType source);

    List<TaskTypeDto> mapToTaskTypeDtos(List<TaskType> source);

    BaseLocation mapToBaseLocation(BaseLocationDto source);

    BaseLocationDto mapToBaseLocationDto(BaseLocation source);

    List<BaseLocationDto> mapToBaseLocationDtos(List<BaseLocation> source);

    Employee mapToEmployee(EmployeeSaveDto source, Function<Integer, BaseLocation> idToBaseLocationProxy);

    EmployeeDto mapToEmployeeDto(Employee source);

    Location mapToLocation(LocationDto source);

    LocationDto mapToLocationDto(Location source);

    List<LocationDto> mapToLocationDtos(List<Location> source);

    Task mapToTask(TaskSaveDto source,
                   Function<Long, Location> idToLocationProxy,
                   Function<Integer, TaskType> idToTaskTypeProxy);

    TaskDto mapToTaskDto(Task source);

    List<TaskDto> mapToTaskDtos(List<Task> source);

    Assignment mapToAssignment(AssignmentSaveDto source,
                               Function<UUID, Employee> idToEmployeeProxy,
                               Function<Long, Task> idToTaskProxy,
                               Function<Integer, BaseLocation> idToBaseLocationProxy,
                               Function<Long, Location> idToLocationProxy);

    AssignmentDto mapToAssignmentDto(Assignment source);

    List<AssignmentDto> mapToAssignmentDtos(List<Assignment> source);
}
