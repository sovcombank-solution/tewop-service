package com.anvacon.tewop.service;

import com.anvacon.tewop.dto.AssignmentDto;
import com.anvacon.tewop.mapper.Mapper;
import com.anvacon.tewop.repository.AssignmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class AssignmentServiceImpl implements AssignmentService {

    private final AssignmentRepository repository;

    private final Mapper mapper;

    @Transactional(readOnly = true)
    @Override
    public List<AssignmentDto> findAllByPlannedDateAndEmployeeId(LocalDate plannedDate, UUID employeeId) {
        return mapper.mapToAssignmentDtos(repository
            .findAllByPlannedDateAndEmployeeIdOrderBySerialNumber(plannedDate, employeeId));
    }
}
