package com.anvacon.tewop.service;

import com.anvacon.tewop.dto.AssignmentDto;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface AssignmentService {

    List<AssignmentDto> findAllByPlannedDateAndEmployeeId(LocalDate plannedDate, UUID employeeId);
}
