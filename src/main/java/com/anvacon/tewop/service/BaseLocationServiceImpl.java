package com.anvacon.tewop.service;

import com.anvacon.tewop.dto.BaseLocationDto;
import com.anvacon.tewop.mapper.Mapper;
import com.anvacon.tewop.repository.BaseLocationRepository;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class BaseLocationServiceImpl implements BaseLocationService {

    private final BaseLocationRepository repository;

    private final Mapper mapper;

    @Transactional(readOnly = true)
    @Override
    public Optional<BaseLocationDto> findById(Integer id) {
        return repository.findById(id).map(mapper::mapToBaseLocationDto);
    }

    @Transactional(readOnly = true)
    @Override
    public List<BaseLocationDto> findAll(Sort sort) {
        return mapper.mapToBaseLocationDtos(repository.findAll(sort));
    }

    @Transactional
    @Override
    public BaseLocationDto save(BaseLocationDto baseLocationDto) {
        val baseLocation = repository.saveAndFlush(mapper.mapToBaseLocation(baseLocationDto));
        return mapper.mapToBaseLocationDto(baseLocation);
    }

    @Transactional
    @Override
    public void deleteById(Integer id) {
        repository.deleteById(id);
    }
}
