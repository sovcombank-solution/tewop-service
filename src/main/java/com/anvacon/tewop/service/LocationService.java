package com.anvacon.tewop.service;

import com.anvacon.tewop.dto.LocationDto;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

public interface LocationService {

    Optional<LocationDto> findById(Long id);

    List<LocationDto> findAll(Sort sort);

    LocationDto save(LocationDto locationDto);

    void deleteById(Long id);

    void deleteAllById(List<Long> ids);
}
