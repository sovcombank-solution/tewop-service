package com.anvacon.tewop.service;

import com.anvacon.tewop.dto.TaskDto;

import java.util.List;

public interface TaskGenerationService {

    List<TaskDto> createTasksBasedOnLocationData();
}
