package com.anvacon.tewop.service;

import com.anvacon.tewop.dto.TaskTypeDto;

import java.util.List;
import java.util.Optional;

public interface TaskTypeService {

    Optional<TaskTypeDto> findById(Integer id);

    List<TaskTypeDto> findAll();
}
