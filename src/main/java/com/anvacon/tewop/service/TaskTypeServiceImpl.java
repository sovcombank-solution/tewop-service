package com.anvacon.tewop.service;

import com.anvacon.tewop.dto.TaskTypeDto;
import com.anvacon.tewop.mapper.Mapper;
import com.anvacon.tewop.repository.TaskTypeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class TaskTypeServiceImpl implements TaskTypeService {

    private final TaskTypeRepository repository;

    private final Mapper mapper;

    @Override
    @Transactional(readOnly = true)
    public Optional<TaskTypeDto> findById(Integer id) {
        return repository.findById(id).map(mapper::mapToTaskTypeDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TaskTypeDto> findAll() {
        return mapper.mapToTaskTypeDtos(repository.findAll());
    }
}
