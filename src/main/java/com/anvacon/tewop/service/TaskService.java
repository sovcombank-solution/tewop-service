package com.anvacon.tewop.service;

import com.anvacon.tewop.dto.TaskDto;
import com.anvacon.tewop.dto.TaskFilter;
import com.anvacon.tewop.dto.TaskSaveDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

public interface TaskService {

    Optional<TaskDto> findById(Long id);

    List<TaskDto> findAll();

    List<TaskDto> findAll(TaskFilter filter);

    List<TaskDto> findAll(Sort sort);

    List<TaskDto> findAll(TaskFilter filter, Sort sort);

    Page<TaskDto> findAll(Pageable pageable);

    Page<TaskDto> findAll(TaskFilter filter, Pageable pageable);

    TaskDto save(TaskSaveDto taskSaveDto);

    void deleteById(Long id);

    void deleteAllById(List<Long> ids);
}
