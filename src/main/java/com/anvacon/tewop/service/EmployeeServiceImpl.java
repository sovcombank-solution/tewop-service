package com.anvacon.tewop.service;

import com.anvacon.tewop.dto.EmployeeDto;
import com.anvacon.tewop.dto.EmployeeFilter;
import com.anvacon.tewop.dto.EmployeeSaveDto;
import com.anvacon.tewop.entity.Employee;
import com.anvacon.tewop.entity.enumeration.Degree;
import com.anvacon.tewop.mapper.Mapper;
import com.anvacon.tewop.repository.BaseLocationRepository;
import com.anvacon.tewop.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.anvacon.tewop.specification.EmployeeSpecification.*;

@RequiredArgsConstructor
@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository repository;

    private final BaseLocationRepository baseLocationRepository;

    private final Mapper mapper;

    @Transactional(readOnly = true)
    @Override
    public Optional<EmployeeDto> findById(UUID id) {
        return repository.findById(id).map(mapper::mapToEmployeeDto);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<EmployeeDto> findAll(Pageable pageable) {
        return repository.findAll(pageable).map(mapper::mapToEmployeeDto);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<EmployeeDto> findAll(EmployeeFilter filter, Pageable pageable) {
        if (filter == null || filter.isEmpty()) {
            return repository.findAll(pageable).map(mapper::mapToEmployeeDto);
        }
        val baseLocationId = filter.getBaseLocationId();
        val baseLocation = baseLocationId != null ?
            baseLocationRepository.findById(baseLocationId).orElse(null) : null;
        Specification<Employee> spec = all()
            .and(hasId(filter.getId()))
            .and(hasPersonnelNumber(filter.getPersonnelNumber()))
            .and(firstNameContainsIgnoreCase(filter.getFirstName()))
            .and(lastNameContainsIgnoreCase(filter.getLastName()))
            .and(fioContainsIgnoreCase(filter.getFio()))
            .and(hasEmail(filter.getEmail()))
            .and(hasDegree(Degree.valueOf(filter.getDegreeId())))
            .and(hasBaseLocation(baseLocation))
            .and(employee(filter.getEmployee()))
            .and(available(filter.getAvailable()))
            .and(agent(filter.getAgent()));
        return repository.findAll(spec, pageable).map(mapper::mapToEmployeeDto);
    }

    @Transactional
    @Override
    public EmployeeDto save(EmployeeSaveDto employeeSaveDto) {
        val employee = repository.save(mapper.mapToEmployee(employeeSaveDto,
            baseLocationRepository::getReferenceById));
        return mapper.mapToEmployeeDto(employee);
    }

    @Transactional
    @Override
    public void deleteById(UUID id) {
        repository.deleteById(id);
    }

    @Transactional
    @Override
    public void deleteAllById(List<UUID> ids) {
        repository.deleteAllById(ids);
    }
}
