package com.anvacon.tewop.service;

import com.anvacon.tewop.dto.EmployeeDto;
import com.anvacon.tewop.dto.EmployeeFilter;
import com.anvacon.tewop.dto.EmployeeSaveDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface EmployeeService {

    Optional<EmployeeDto> findById(UUID id);

    Page<EmployeeDto> findAll(Pageable pageable);

    Page<EmployeeDto> findAll(EmployeeFilter filter, Pageable pageable);

    EmployeeDto save(EmployeeSaveDto employeeSaveDto);

    void deleteById(UUID id);

    void deleteAllById(List<UUID> ids);
}
