package com.anvacon.tewop.service;

import com.anvacon.tewop.dto.LocationDto;
import com.anvacon.tewop.mapper.Mapper;
import com.anvacon.tewop.repository.LocationRepository;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class LocationServiceImpl implements LocationService {

    private final LocationRepository repository;

    private final Mapper mapper;

    @Transactional(readOnly = true)
    @Override
    public Optional<LocationDto> findById(Long id) {
        return repository.findById(id).map(mapper::mapToLocationDto);
    }

    @Transactional(readOnly = true)
    @Override
    public List<LocationDto> findAll(Sort sort) {
        if (sort == null) {
            return mapper.mapToLocationDtos(repository.findAll());
        }
        return mapper.mapToLocationDtos(repository.findAll(sort));
    }

    @Transactional
    @Override
    public LocationDto save(LocationDto locationDto) {
        val location = repository.saveAndFlush(mapper.mapToLocation(locationDto));
        return mapper.mapToLocationDto(location);
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Transactional
    @Override
    public void deleteAllById(List<Long> ids) {
        repository.deleteAllById(ids);
    }
}
