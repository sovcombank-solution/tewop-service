package com.anvacon.tewop.service;

import com.anvacon.tewop.dto.TaskDto;
import com.anvacon.tewop.entity.Task;
import com.anvacon.tewop.mapper.Mapper;
import com.anvacon.tewop.planner.processor.*;
import com.anvacon.tewop.repository.LocationRepository;
import com.anvacon.tewop.repository.TaskRepository;
import com.anvacon.tewop.repository.TaskTypeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

import static com.anvacon.tewop.specification.TaskSpecification.assigned;

@Slf4j
@RequiredArgsConstructor
@Service
public class TaskGenerationServiceImpl implements TaskGenerationService {

    private static final String MISSING_TASK_TYPES = "Отсутствуют метаданные: типы задач";

    private final LocationRepository locationRepository;
    private final TaskRepository taskRepository;
    private final TaskTypeRepository taskTypeRepository;
    private final Mapper mapper;

    @Transactional
    @Override
    public List<TaskDto> createTasksBasedOnLocationData() {
        val promotionOfIssuanceTask = taskTypeRepository.findById(1);
        val agentTrainingTask = taskTypeRepository.findById(2);
        val deliveryOfMaterialsTask = taskTypeRepository.findById(3);
        if (promotionOfIssuanceTask.isEmpty() || agentTrainingTask.isEmpty() || deliveryOfMaterialsTask.isEmpty()) {
            log.error(MISSING_TASK_TYPES);
            throw new RuntimeException(MISSING_TASK_TYPES);
        }
        final RuleProcessor ruleProcessor = new SevenDaysRuleProcessor(promotionOfIssuanceTask.get())
            .setNext(new FourteenDaysRuleProcessor(promotionOfIssuanceTask.get()))
            .setNext(new LessThanFiftyPercentRuleProcessor(agentTrainingTask.get()))
            .setNext(new NewLocationRuleProcessor(deliveryOfMaterialsTask.get()))
            .setNext(new NoMaterialsRuleProcessor(deliveryOfMaterialsTask.get()));
        Specification<Task> spec = assigned(false);
        taskRepository.delete(spec);
        val locations = locationRepository
            .findAllByTakenIntoAccountWhenPlanningIsTrueOrderByExternalId();
        List<Task> tasks = locations.stream()
            .map(location -> {
                val taskType = ruleProcessor.apply(location);
                if (taskType == null) {
                    return null;
                }
                return Task.builder()
                    .location(location)
                    .taskType(taskType)
                    .assigned(false)
                    .canceled(false)
                    .overdue(false)
                    .currentPriority(taskType.getPriority())
                    .completed(false)
                    .build();
            })
            .filter(Objects::nonNull)
            .toList();
        return taskRepository.saveAllAndFlush(tasks).stream()
            .map(mapper::mapToTaskDto)
            .toList();
    }
}
