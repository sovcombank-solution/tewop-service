package com.anvacon.tewop.service;

import com.anvacon.tewop.dto.BaseLocationDto;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

public interface BaseLocationService {

    Optional<BaseLocationDto> findById(Integer id);

    List<BaseLocationDto> findAll(Sort sort);

    BaseLocationDto save(BaseLocationDto baseLocationDto);

    void deleteById(Integer id);
}
