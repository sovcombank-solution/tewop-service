package com.anvacon.tewop.service;

import com.anvacon.tewop.dto.TaskDto;
import com.anvacon.tewop.dto.TaskFilter;
import com.anvacon.tewop.dto.TaskSaveDto;
import com.anvacon.tewop.entity.Task;
import com.anvacon.tewop.entity.enumeration.Priority;
import com.anvacon.tewop.mapper.Mapper;
import com.anvacon.tewop.repository.LocationRepository;
import com.anvacon.tewop.repository.TaskRepository;
import com.anvacon.tewop.repository.TaskTypeRepository;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static com.anvacon.tewop.specification.TaskSpecification.*;

@RequiredArgsConstructor
@Service
public class TaskServiceImpl implements TaskService {

    private final TaskRepository repository;

    private final TaskTypeRepository taskTypeRepository;

    private final LocationRepository locationRepository;

    private final Mapper mapper;

    @Transactional(readOnly = true)
    @Override
    public Optional<TaskDto> findById(Long id) {
        return repository.findById(id).map(mapper::mapToTaskDto);
    }

    @Override
    public List<TaskDto> findAll() {
        return mapper.mapToTaskDtos(repository.findAll());
    }

    @Transactional(readOnly = true)
    @Override
    public List<TaskDto> findAll(TaskFilter filter) {
        if (filter == null || filter.isEmpty()) {
            return mapper.mapToTaskDtos(repository.findAll());
        }
        return mapper.mapToTaskDtos(repository.findAll(newSpecification(filter)));
    }

    @Transactional(readOnly = true)
    @Override
    public List<TaskDto> findAll(Sort sort) {
        if (sort == null) {
            return mapper.mapToTaskDtos(repository.findAll());
        }
        return mapper.mapToTaskDtos(repository.findAll(sort));
    }

    @Transactional(readOnly = true)
    @Override
    public List<TaskDto> findAll(TaskFilter filter, Sort sort) {
        if (filter == null || filter.isEmpty()) {
            return mapper.mapToTaskDtos(repository.findAll(sort));
        }
        return mapper.mapToTaskDtos(repository.findAll(newSpecification(filter), sort));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<TaskDto> findAll(Pageable pageable) {
        return repository.findAll(pageable).map(mapper::mapToTaskDto);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<TaskDto> findAll(TaskFilter filter, Pageable pageable) {
        if (filter == null || filter.isEmpty()) {
            return repository.findAll(pageable).map(mapper::mapToTaskDto);
        }
        return repository.findAll(newSpecification(filter), pageable).map(mapper::mapToTaskDto);
    }

    @Transactional
    @Override
    public TaskDto save(TaskSaveDto taskSaveDto) {
        val task = repository.saveAndFlush(mapper.mapToTask(taskSaveDto,
            locationRepository::getReferenceById, taskTypeRepository::getReferenceById));
        return mapper.mapToTaskDto(task);
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Transactional
    @Override
    public void deleteAllById(List<Long> ids) {
        repository.deleteAllById(ids);
    }

    private Specification<Task> newSpecification(TaskFilter filter) {
        return all()
            .and(hasId(filter.getId()))
            .and(assigned(filter.getAssigned()))
            .and(hasCurrentPriority(Priority.valueOf(filter.getCurrentPriorityId())));
    }
}
