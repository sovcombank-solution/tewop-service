package com.anvacon.tewop.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import java.util.UUID;

import static org.springframework.util.StringUtils.hasText;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class EmployeeFilter {

    private UUID id;
    private String personnelNumber;
    private String firstName;
    private String lastName;
    private String fio;
    private String email;
    private Integer degreeId;
    private Integer baseLocationId;
    private Boolean employee;
    private Boolean available;
    private Boolean agent;

    @JsonIgnore
    public boolean isEmpty() {
        return id == null
            && !hasText(personnelNumber)
            && !hasText(firstName)
            && !hasText(lastName)
            && !hasText(fio)
            && !hasText(email)
            && degreeId == null
            && baseLocationId == null
            && employee == null
            && available == null
            && agent == null;
    }
}
