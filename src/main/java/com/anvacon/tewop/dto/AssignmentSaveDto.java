package com.anvacon.tewop.dto;

import lombok.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class AssignmentSaveDto {
    private Long id;
    private LocalDate plannedDate;
    private UUID employeeId;
    private Long taskId;
    private LocalTime startTime;
    private int serialNumber;
    private Integer startingBaseLocationId;
    private Long startingLocationId;
    private int distance;
    private int routeDuration;
    private int cumulativeDuration;
    private boolean canceled;
    private boolean failed;
    private boolean completed;
}
