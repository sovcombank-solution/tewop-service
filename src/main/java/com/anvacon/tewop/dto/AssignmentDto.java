package com.anvacon.tewop.dto;

import com.anvacon.tewop.entity.Assignment;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * A DTO for the {@link Assignment} entity
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class AssignmentDto implements Serializable {
    private Long id;
    private LocalDate plannedDate;
    private EmployeeDto employee;
    private TaskDto task;
    private LocalTime startTime;
    private int serialNumber;
    private BaseLocationDto startingBaseLocation;
    private LocationDto startingLocation;
    private int distance;
    private int routeDuration;
    private int cumulativeDuration;
    private boolean canceled;
    private boolean failed;
    private boolean completed;
}
