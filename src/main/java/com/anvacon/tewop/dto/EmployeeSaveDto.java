package com.anvacon.tewop.dto;

import lombok.*;

import java.io.Serializable;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class EmployeeSaveDto implements Serializable {
    private UUID id;
    private String personnelNumber;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String email;
    private String avatar;
    private Integer degreeId;
    private Integer baseLocationId;
    private boolean employee;
    private boolean available;
    private boolean agent;
}
