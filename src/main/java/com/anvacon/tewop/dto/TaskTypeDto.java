package com.anvacon.tewop.dto;

import com.anvacon.tewop.entity.enumeration.Degree.DegreeDto;
import com.anvacon.tewop.entity.enumeration.Priority.PriorityDto;
import lombok.*;

import java.io.Serializable;

/**
 * A DTO for the {@link com.anvacon.tewop.entity.TaskType} entity
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class TaskTypeDto implements Serializable {
    private Integer id;
    private String name;
    private PriorityDto priority;
    private Integer duration;
    private DegreeDto minimumDegree;
}
