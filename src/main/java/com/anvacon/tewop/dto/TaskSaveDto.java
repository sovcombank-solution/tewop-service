package com.anvacon.tewop.dto;


import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class TaskSaveDto {
    private Long id;
    private Long locationId;
    private Integer taskTypeId;
    private boolean assigned;
    private boolean canceled;
    private boolean overdue;
    private Integer currentPriorityId;
    private boolean completed;
}
