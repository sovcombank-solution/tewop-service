package com.anvacon.tewop.dto.algorithm;

import com.anvacon.tewop.entity.BaseLocation;
import com.anvacon.tewop.entity.Employee;
import com.anvacon.tewop.entity.Location;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EmployeeDailyDto {
    private LocalDate plannedDate;
    private Employee employee;
    private int serialNumber;
    private int cumulativeDuration;
    private BaseLocation startingBaseLocation;
    private Location startingLocation;
    private boolean dayCompleted;
}
