package com.anvacon.tewop.dto.sso;

import lombok.*;

/**
 * Профиль пользователя получаемый из авторизационного токена,
 * чтобы все поля были записаны в токен сервером авторизации
 * необходимо, чтобы при получении токена были запрошены следующие
 * scopes: profile (включен по умолчанию), email (включен по умолчанию)
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class OpenIDConnectUserProfile {
    private String uniquePrincipalName;
    private String email;
    private Boolean emailVerified;
    private String familyName;
    private String givenName;
    private String patronymic;
    private String picture;
}
