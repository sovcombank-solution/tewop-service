package com.anvacon.tewop.dto.sso;

import lombok.*;

import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class OpenIDConnectUser {
    private UUID id;
    private OpenIDConnectUserProfile profile;
    private List<String> authorities;
}
