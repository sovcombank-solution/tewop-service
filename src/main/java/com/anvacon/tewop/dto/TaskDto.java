package com.anvacon.tewop.dto;

import com.anvacon.tewop.entity.enumeration.Priority.PriorityDto;
import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class TaskDto implements Serializable {
    private Long id;
    private LocationDto location;
    private TaskTypeDto taskType;
    private boolean assigned;
    private boolean canceled;
    private boolean overdue;
    private PriorityDto currentPriority;
    private boolean completed;
}
