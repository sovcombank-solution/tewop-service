package com.anvacon.tewop.dto.geo;

public record RouteInfoBrief(int distance, int duration) {
}
