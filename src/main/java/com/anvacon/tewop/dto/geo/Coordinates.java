package com.anvacon.tewop.dto.geo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Coordinates {

    private BigDecimal longitude;

    private BigDecimal latitude;
}
