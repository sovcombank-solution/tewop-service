package com.anvacon.tewop.dto;

import com.anvacon.tewop.entity.enumeration.Degree.DegreeDto;
import lombok.*;

import java.io.Serializable;
import java.util.UUID;

/**
 * A DTO for the {@link com.anvacon.tewop.entity.Employee} entity
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class EmployeeDto implements Serializable {
    private UUID id;
    private String personnelNumber;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String email;
    private String avatar;
    private DegreeDto degree;
    private BaseLocationDto baseLocation;
    private boolean employee;
    private boolean available;
    private boolean agent;
}
