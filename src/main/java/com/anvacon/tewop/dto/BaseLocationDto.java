package com.anvacon.tewop.dto;

import lombok.*;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class BaseLocationDto {
    private Integer id;
    private String address;
    private BigDecimal longitude;
    private BigDecimal latitude;
    private String description;
}
