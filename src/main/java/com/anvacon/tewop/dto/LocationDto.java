package com.anvacon.tewop.dto;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A DTO for the {@link com.anvacon.tewop.entity.Location} entity
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class LocationDto implements Serializable {
    private Long id;
    /**
     * Номер точки из исходных данных
     */
    private Long externalId;
    private String address;
    private BigDecimal longitude;
    private BigDecimal latitude;
    /**
     * Новая точка,
     * true - новая (подключена вчера)
     * false - не новая (подключена давно)
     */
    private boolean newPlace;
    /**
     * Карты и материалы доставлены
     */
    private boolean cardsAndDocumentsDelivered;
    /**
     * Количество дней после выдачи последней карты
     */
    private Integer daysAfterIssuance;
    /**
     * Количество одобренных заявок
     */
    private Integer approvedApplications;
    /**
     * Количество выданных карт
     */
    private Integer cardsIssued;
    private boolean takenIntoAccountWhenPlanning;
}
