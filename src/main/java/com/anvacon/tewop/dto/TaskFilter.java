package com.anvacon.tewop.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class TaskFilter {

    private Long id;
    private Boolean assigned;
    private Integer currentPriorityId;

    @JsonIgnore
    public boolean isEmpty() {
        return id == null
            && assigned == null
            && currentPriorityId == null;
    }
}
