package com.anvacon.tewop.planner.matrix;

import com.anvacon.tewop.dto.geo.RouteInfoBrief;
import com.anvacon.tewop.entity.BaseLocation;
import com.anvacon.tewop.entity.Location;

public interface DistanceMatrix {

    RouteInfoBrief getRouteInfo(BaseLocation startBaseLocation,
                                Location startLocation,
                                Location targetLocation);
}
