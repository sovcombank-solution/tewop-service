package com.anvacon.tewop.planner.processor;

import com.anvacon.tewop.entity.Location;
import com.anvacon.tewop.entity.TaskType;

public abstract class RuleProcessor {

    protected final TaskType taskType;
    private RuleProcessor nextProcessor;

    public RuleProcessor(TaskType taskType) {
        this.taskType = taskType;
    }

    public RuleProcessor setNext(RuleProcessor processor) {
        nextProcessor = processor;
        return nextProcessor;
    }

    public abstract TaskType apply(Location location);

    protected TaskType applyNext(Location location) {
        if (nextProcessor != null) {
            return nextProcessor.apply(location);
        }
        return null;
    }
}
