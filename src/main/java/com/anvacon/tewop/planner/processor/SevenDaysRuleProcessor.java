package com.anvacon.tewop.planner.processor;

import com.anvacon.tewop.entity.Location;
import com.anvacon.tewop.entity.TaskType;

public class SevenDaysRuleProcessor extends RuleProcessor {

    public SevenDaysRuleProcessor(TaskType taskType) {
        super(taskType);
    }

    @Override
    public TaskType apply(Location location) {
        if (location.getDaysAfterIssuance() > 7 && location.getApprovedApplications() > 0) {
            return taskType;
        }
        return applyNext(location);
    }
}
