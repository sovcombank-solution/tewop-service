package com.anvacon.tewop.planner.processor;

import com.anvacon.tewop.entity.Location;
import com.anvacon.tewop.entity.TaskType;

public class LessThanFiftyPercentRuleProcessor extends RuleProcessor {

    public LessThanFiftyPercentRuleProcessor(TaskType taskType) {
        super(taskType);
    }

    @Override
    public TaskType apply(Location location) {
        if (isLessThanFiftyPercent(location)) {
            return taskType;
        }
        return applyNext(location);
    }

    private boolean isLessThanFiftyPercent(Location location) {
        double cardsIssued = location.getCardsIssued();
        double approvedApplications = location.getApprovedApplications();
        return location.getCardsIssued() > 0 &&
            (approvedApplications == 0 || cardsIssued / approvedApplications < 0.5);
    }
}
