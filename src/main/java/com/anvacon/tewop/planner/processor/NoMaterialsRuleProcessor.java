package com.anvacon.tewop.planner.processor;

import com.anvacon.tewop.entity.Location;
import com.anvacon.tewop.entity.TaskType;

public class NoMaterialsRuleProcessor extends RuleProcessor {

    public NoMaterialsRuleProcessor(TaskType taskType) {
        super(taskType);
    }

    @Override
    public TaskType apply(Location location) {
        if (!location.isCardsAndDocumentsDelivered()) {
            return taskType;
        }
        return applyNext(location);
    }
}
