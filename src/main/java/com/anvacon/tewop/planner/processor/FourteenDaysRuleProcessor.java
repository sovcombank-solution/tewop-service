package com.anvacon.tewop.planner.processor;

import com.anvacon.tewop.entity.Location;
import com.anvacon.tewop.entity.TaskType;

public class FourteenDaysRuleProcessor extends RuleProcessor {

    public FourteenDaysRuleProcessor(TaskType taskType) {
        super(taskType);
    }

    @Override
    public TaskType apply(Location location) {
        if (location.getDaysAfterIssuance() > 14) {
            return taskType;
        }
        return applyNext(location);
    }
}
