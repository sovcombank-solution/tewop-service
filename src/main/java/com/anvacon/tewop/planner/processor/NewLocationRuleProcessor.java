package com.anvacon.tewop.planner.processor;

import com.anvacon.tewop.entity.Location;
import com.anvacon.tewop.entity.TaskType;

public class NewLocationRuleProcessor extends RuleProcessor {

    public NewLocationRuleProcessor(TaskType taskType) {
        super(taskType);
    }

    @Override
    public TaskType apply(Location location) {
        if (location.isNewPlace()) {
            return taskType;
        }
        return applyNext(location);
    }
}
