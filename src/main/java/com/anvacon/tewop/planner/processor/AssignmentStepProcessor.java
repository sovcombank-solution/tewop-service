package com.anvacon.tewop.planner.processor;

import com.anvacon.tewop.dto.algorithm.EmployeeDailyDto;
import com.anvacon.tewop.entity.Assignment;
import com.anvacon.tewop.entity.Task;
import com.anvacon.tewop.planner.matrix.DistanceMatrix;

import java.util.List;

public interface AssignmentStepProcessor {

    List<Assignment> process(List<EmployeeDailyDto> employees,
                             List<Task> tasks,
                             DistanceMatrix distanceMatrix);
}
