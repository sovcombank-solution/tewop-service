package com.anvacon.tewop.planner.processor;

import com.anvacon.tewop.dto.algorithm.EmployeeDailyDto;
import com.anvacon.tewop.entity.Assignment;
import com.anvacon.tewop.entity.Task;
import com.anvacon.tewop.planner.matrix.DistanceMatrix;
import com.google.ortools.Loader;
import com.google.ortools.sat.*;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class AssignmentStepProcessorImpl implements AssignmentStepProcessor {

    public static final LocalTime WORK_DAY_BEGINNING = LocalTime.of(9, 0);
    public static final int WORK_DAY_DURATION = 9 * 60;

    @Override
    public List<Assignment> process(List<EmployeeDailyDto> employees,
                                    List<Task> tasks,
                                    DistanceMatrix distanceMatrix) {
        Loader.loadNativeLibraries();
        final int numEmployees = employees.size();
        final int numTasks = tasks.size();

        // Data
        int[][] costs = new int[numEmployees][numTasks];
        for (int employeeIndex = 0; employeeIndex < numEmployees; employeeIndex++) {
            for (int taskIndex = 0; taskIndex < numTasks; taskIndex++) {
                val employee = employees.get(employeeIndex);
                val task = tasks.get(taskIndex);
                costs[employeeIndex][taskIndex] = distanceMatrix.getRouteInfo(
                        employee.getStartingBaseLocation(),
                        employee.getStartingLocation(),
                        task.getLocation())
                    .duration();
            }
        }

        // Model
        CpModel model = new CpModel();

        // Variables
        Literal[][] x = new Literal[numEmployees][numTasks];
        for (int employeeIndex = 0; employeeIndex < numEmployees; employeeIndex++) {
            for (int taskIndex = 0; taskIndex < numTasks; taskIndex++) {
                x[employeeIndex][taskIndex] = model
                    .newBoolVar("x[" + employeeIndex + "," + taskIndex + "]");
            }
        }

        // Constraints
        // Each worker is assigned to at most one task.
        for (int employeeIndex = 0; employeeIndex < numEmployees; employeeIndex++) {
            List<Literal> taskLiterals = new ArrayList<>();
            //noinspection ManualArrayToCollectionCopy
            for (int taskIndex = 0; taskIndex < numTasks; taskIndex++) {
                taskLiterals.add(x[employeeIndex][taskIndex]);
            }
            model.addAtMostOne(taskLiterals);
        }

        // Each task is assigned to exactly one worker.
        for (int taskIndex = 0; taskIndex < numTasks; taskIndex++) {
            List<Literal> employeesLiteral = new ArrayList<>();
            for (int employeeIndex = 0; employeeIndex < numEmployees; employeeIndex++) {
                employeesLiteral.add(x[employeeIndex][taskIndex]);
            }
            model.addExactlyOne(employeesLiteral);
        }

        // Objective
        LinearExprBuilder obj = LinearExpr.newBuilder();
        for (int employeeIndex = 0; employeeIndex < numEmployees; employeeIndex++) {
            for (int taskIndex = 0; taskIndex < numTasks; taskIndex++) {
                obj.addTerm(x[employeeIndex][taskIndex], costs[employeeIndex][taskIndex]);
            }
        }
        model.minimize(obj);

        // Solve
        CpSolver solver = new CpSolver();
        CpSolverStatus status = solver.solve(model);

        if (status != CpSolverStatus.OPTIMAL && status != CpSolverStatus.FEASIBLE) {
            log.error("No solution found.");
            throw new RuntimeException("No solution found.");
        }

        val assignments = new ArrayList<Assignment>();
        for (int employeeIndex = 0; employeeIndex < numEmployees; employeeIndex++) {
            for (int taskIndex = 0; taskIndex < numTasks; taskIndex++) {
                if (!solver.booleanValue(x[employeeIndex][taskIndex])) {
                    continue;
                }
                val employee = employees.get(employeeIndex);
                val task = tasks.get(taskIndex);
                final int totalDuration = employee.getCumulativeDuration() +
                    costs[employeeIndex][taskIndex] + task.getTaskType().getDuration();
                final int serialNumber = employee.getSerialNumber() + 1;
                val startingBaseLocation = employee.getStartingBaseLocation();
                val startingLocation = employee.getStartingLocation();
                val distance = distanceMatrix.getRouteInfo(
                        startingBaseLocation,
                        startingLocation,
                        task.getLocation())
                    .distance();
                if (totalDuration > WORK_DAY_DURATION) {
                    employee.setDayCompleted(true);
                    continue;
                }
                // устанавливаем стартовые параметры для следующего шага
                employee.setSerialNumber(serialNumber);
                employee.setCumulativeDuration(totalDuration);
                employee.setStartingBaseLocation(null);
                employee.setStartingLocation(task.getLocation());
                employee.setDayCompleted(false);
                // создаем задание
                val assignment = Assignment.builder()
                    .plannedDate(employee.getPlannedDate())
                    .employee(employee.getEmployee())
                    .task(task)
                    .startTime(WORK_DAY_BEGINNING.plusMinutes(totalDuration))
                    .serialNumber(serialNumber)
                    .startingBaseLocation(startingBaseLocation)
                    .startingLocation(startingLocation)
                    .distance(distance)
                    .routeDuration(costs[employeeIndex][taskIndex])
                    .cumulativeDuration(totalDuration)
                    .canceled(false)
                    .failed(false)
                    .completed(false)
                    .build();
                assignments.add(assignment);
                // устанавливаем для задачи флаг назначена исполнителю
                task.setAssigned(true);
            }
        }
        return assignments;
    }
}
