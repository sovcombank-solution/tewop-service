package com.anvacon.tewop.converter;

import com.anvacon.tewop.entity.enumeration.Priority;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter(autoApply = true)
public class PriorityConverter implements AttributeConverter<Priority, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Priority value) {
        if (value == null) {
            return null;
        }
        return value.getId();
    }

    @Override
    public Priority convertToEntityAttribute(Integer id) {
        return Priority.valueOf(id);
    }
}
