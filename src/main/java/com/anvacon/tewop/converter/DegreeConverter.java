package com.anvacon.tewop.converter;

import com.anvacon.tewop.entity.enumeration.Degree;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter(autoApply = true)
public class DegreeConverter implements AttributeConverter<Degree, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Degree value) {
        if (value == null) {
            return null;
        }
        return value.getId();
    }

    @Override
    public Degree convertToEntityAttribute(Integer id) {
        return Degree.valueOf(id);
    }
}
