package com.anvacon.tewop.converter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

@Slf4j
public record JwtRolesGrantedAuthoritiesConverter(String rolesClaimName)
    implements Converter<Jwt, Collection<GrantedAuthority>> {

    @Override
    public Collection<GrantedAuthority> convert(@NonNull Jwt source) {
        if (!source.hasClaim(rolesClaimName)) {
            log.trace(String.format("Returning no authorities since could not find %s claim", rolesClaimName));
            return Collections.emptyList();
        }
        if (log.isTraceEnabled()) {
            log.trace(String.format("Looking for roles in claim %s", rolesClaimName));
        }
        return source.getClaimAsStringList(rolesClaimName).stream()
            .filter(StringUtils::hasText)
            .map(SimpleGrantedAuthority::new)
            .collect(Collectors.toUnmodifiableSet());
    }
}
