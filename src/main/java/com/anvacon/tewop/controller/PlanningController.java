package com.anvacon.tewop.controller;

import com.anvacon.tewop.dto.TaskDto;
import com.anvacon.tewop.service.TaskGenerationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.anvacon.tewop.Names.SECURITY_SCHEME;

@Tag(name = "Planning", description = "Планирование")
@RequiredArgsConstructor
@RestController
@SecurityRequirement(name = SECURITY_SCHEME)
public class PlanningController {

    private final TaskGenerationService service;

    @Operation(summary = "Create tasks based on Location data")
    @PostMapping("api/v1/planning/generate-tasks")
    public ResponseEntity<List<TaskDto>> generateTasks() {
        return ResponseEntity.ok(generate());
    }

    private synchronized List<TaskDto> generate() {
        return service.createTasksBasedOnLocationData();
    }
}
