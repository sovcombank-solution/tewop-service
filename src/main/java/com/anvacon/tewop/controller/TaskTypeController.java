package com.anvacon.tewop.controller;

import com.anvacon.tewop.dto.TaskTypeDto;
import com.anvacon.tewop.service.TaskTypeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Tag(name = "Task types", description = "Типы задач")
@RequiredArgsConstructor
@RestController
public class TaskTypeController {

    private final TaskTypeService service;

    @Operation(summary = "Get all task types")
    @GetMapping("/api/v1/task-types")
    public List<TaskTypeDto> getAllTaskTypes() {
        return service.findAll();
    }

    @Operation(summary = "Get task type by id")
    @GetMapping("/api/v1/task-types/{id}")
    public ResponseEntity<TaskTypeDto> getTaskTypeById(@PathVariable("id") Integer id) {
        return ResponseEntity.of(service.findById(id));
    }
}
