package com.anvacon.tewop.controller;

import com.anvacon.tewop.dto.BaseLocationDto;
import com.anvacon.tewop.service.BaseLocationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.anvacon.tewop.Names.SECURITY_SCHEME;
import static org.springframework.data.domain.Sort.Direction.ASC;

@Tag(name = "Base locations", description = "Базовые локации сотрудников")
@RequiredArgsConstructor
@RestController
@SecurityRequirement(name = SECURITY_SCHEME)
public class BaseLocationController {

    private final BaseLocationService service;

    @Operation(summary = "Get all Base locations")
    @Parameter(in = ParameterIn.QUERY,
        description = "Sorting criteria in the format: property(,asc|desc). " +
            "Default sort order is ascending. " +
            "Multiple sort criteria are supported.",
        name = "sort",
        required = false,
        array = @ArraySchema(schema = @Schema(type = "string", defaultValue = "id,asc")))
    @GetMapping("/api/v1/base-locations")
    public ResponseEntity<List<BaseLocationDto>> getAllBaseLocations(
        @Parameter(hidden = true) @SortDefault(sort = "id", direction = ASC) Sort sort
    ) {
        return ResponseEntity.ok(service.findAll(sort));
    }

    @Operation(summary = "Get Base location by id")
    @GetMapping("/api/v1/base-locations/{id}")
    public ResponseEntity<BaseLocationDto> getBaseLocationById(@PathVariable Integer id) {
        return ResponseEntity.of(service.findById(id));
    }

    @Operation(summary = "Update Base location and create if not exists")
    @PostMapping("api/v1/base-locations")
    public ResponseEntity<BaseLocationDto> mergeBaseLocation(@Valid @RequestBody BaseLocationDto dto) {
        return ResponseEntity.ok(service.save(dto));
    }

    @Operation(summary = "Delete Base location by id")
    @DeleteMapping("api/v1/base-locations/{id}")
    public void deleteBaseLocationById(@PathVariable Integer id) {
        service.deleteById(id);
    }
}
