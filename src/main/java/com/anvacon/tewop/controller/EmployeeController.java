package com.anvacon.tewop.controller;

import com.anvacon.tewop.dto.EmployeeDto;
import com.anvacon.tewop.dto.EmployeeFilter;
import com.anvacon.tewop.dto.EmployeeSaveDto;
import com.anvacon.tewop.service.EmployeeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static com.anvacon.tewop.Names.SECURITY_SCHEME;

@Tag(name = "Employees", description = "Сотрудники")
@RequiredArgsConstructor
@RestController
@SecurityRequirement(name = SECURITY_SCHEME)
public class EmployeeController {

    private final EmployeeService service;

    @Operation(summary = "Get all employees")
    @Parameter(in = ParameterIn.QUERY,
        description = "Sorting criteria in the format: property(,asc|desc). " +
            "Default sort order is ascending. " +
            "Multiple sort criteria are supported.",
        name = "sort",
        required = false,
        array = @ArraySchema(schema = @Schema(type = "string", defaultValue = "personnelNumber,asc")))
    @GetMapping("/api/v1/employees")
    public ResponseEntity<Page<EmployeeDto>> getAllEmployees(
        @RequestParam(required = false) UUID id,
        @RequestParam(required = false) String personnelNumber,
        @RequestParam(required = false) String firstName,
        @RequestParam(required = false) String lastName,
        @RequestParam(required = false) String fio,
        @RequestParam(required = false) String email,
        @RequestParam(required = false) Integer degreeId,
        @RequestParam(required = false) Integer baseLocationId,
        @RequestParam(required = false) Boolean employee,
        @RequestParam(required = false) Boolean available,
        @RequestParam(required = false) Boolean agent,
        @RequestParam(required = false, defaultValue = "0") Integer page,
        @RequestParam(required = false, defaultValue = "10") Integer size,
        @Parameter(hidden = true) @PageableDefault(size = 10, sort = "personnelNumber") Pageable pageable
    ) {
        EmployeeFilter filter = EmployeeFilter.builder()
            .id(id)
            .personnelNumber(personnelNumber)
            .firstName(firstName)
            .lastName(lastName)
            .fio(fio)
            .email(email)
            .degreeId(degreeId)
            .baseLocationId(baseLocationId)
            .employee(employee)
            .available(available)
            .agent(agent)
            .build();
        return ResponseEntity.ok(service.findAll(filter, pageable));
    }

    @Operation(summary = "Get employee by id")
    @GetMapping("/api/v1/employees/{id}")
    public ResponseEntity<EmployeeDto> getEmployeeById(@PathVariable UUID id) {
        return ResponseEntity.of(service.findById(id));
    }

    @Operation(summary = "Update employee and create if not exists")
    @PostMapping("api/v1/employees")
    public ResponseEntity<EmployeeDto> mergeEmployee(@Valid @RequestBody EmployeeSaveDto dto) {
        return ResponseEntity.ok(service.save(dto));
    }

    @Operation(summary = "Delete employee by id")
    @DeleteMapping("api/v1/employees/{id}")
    public void deleteEmployeeById(@PathVariable UUID id) {
        service.deleteById(id);
    }

    @Operation(summary = "Delete employees by ids")
    @DeleteMapping("api/v1/employees")
    public void deleteAllEmployeesByIds(@Valid @RequestBody List<UUID> ids) {
        service.deleteAllById(ids);
    }
}
