package com.anvacon.tewop.controller;

import com.anvacon.tewop.dto.AssignmentDto;
import com.anvacon.tewop.dto.sso.OpenIDConnectUser;
import com.anvacon.tewop.service.AssignmentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.anvacon.tewop.Names.SECURITY_SCHEME;
import static com.anvacon.tewop.advice.SecurityControllerAdvice.OIDC_USER_INFO;

@Tag(name = "Assignments", description = "Задания")
@RequiredArgsConstructor
@RestController
@SecurityRequirement(name = SECURITY_SCHEME)
public class AssignmentController {

    private final AssignmentService service;

    @Operation(summary = "Get all employee assignments for a date")
    @GetMapping("/api/v1/assignments")
    public ResponseEntity<List<AssignmentDto>> getAllAssignmentsByDateAndEmployeeId(
        @RequestParam LocalDate plannedDate,
        @RequestParam UUID employeeId,
        @Parameter(hidden = true) @ModelAttribute(OIDC_USER_INFO) Optional<OpenIDConnectUser> userOptional
    ) {
        val user = userOptional.orElseThrow();
        if (user.getAuthorities().contains("ROLE_MANAGER")) {
            return ResponseEntity.ok(service.findAllByPlannedDateAndEmployeeId(plannedDate, employeeId));
        }
        if (user.getId().equals(employeeId)) {
            return ResponseEntity.ok(service.findAllByPlannedDateAndEmployeeId(plannedDate, user.getId()));
        }
        return ResponseEntity.ok(Collections.emptyList());
    }
}
