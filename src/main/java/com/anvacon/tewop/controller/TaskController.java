package com.anvacon.tewop.controller;

import com.anvacon.tewop.dto.TaskDto;
import com.anvacon.tewop.dto.TaskFilter;
import com.anvacon.tewop.service.TaskService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.anvacon.tewop.Names.SECURITY_SCHEME;
import static org.springframework.data.domain.Sort.Direction.ASC;

@Tag(name = "Tasks", description = "Задачи")
@RequiredArgsConstructor
@RestController
@SecurityRequirement(name = SECURITY_SCHEME)
public class TaskController {

    private final TaskService service;

    @Operation(summary = "Get all tasks")
    @Parameter(in = ParameterIn.QUERY,
        description = "Sorting criteria in the format: property(,asc|desc). " +
            "Default sort order is ascending. " +
            "Multiple sort criteria are supported.",
        name = "sort",
        required = false,
        array = @ArraySchema(schema = @Schema(type = "Long", defaultValue = "location_id,asc")))
    @GetMapping("/api/v1/tasks")
    public ResponseEntity<List<TaskDto>> getAllTasks(
        @RequestParam(required = false) Boolean assigned,
        @RequestParam(required = false) Integer currentPriorityId,
        @Parameter(hidden = true) @SortDefault(sort = "location_id", direction = ASC) Sort sort
    ) {
        TaskFilter filter = TaskFilter.builder()
            .assigned(assigned)
            .currentPriorityId(currentPriorityId)
            .build();
        return ResponseEntity.ok(service.findAll(filter, sort));
    }

    @Operation(summary = "Get task by id")
    @GetMapping("/api/v1/tasks/{id}")
    public ResponseEntity<TaskDto> getTaskById(@PathVariable Long id) {
        return ResponseEntity.of(service.findById(id));
    }
}
