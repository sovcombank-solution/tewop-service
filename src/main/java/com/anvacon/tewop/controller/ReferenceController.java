package com.anvacon.tewop.controller;

import com.anvacon.tewop.entity.enumeration.Degree;
import com.anvacon.tewop.entity.enumeration.Degree.DegreeDto;
import com.anvacon.tewop.entity.enumeration.Priority;
import com.anvacon.tewop.entity.enumeration.Priority.PriorityDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Tag(name = "References", description = "Справочники")
@RequiredArgsConstructor
@RestController
public class ReferenceController {

    @Operation(summary = "Get all degrees")
    @GetMapping("/api/v1/references/degrees")
    public List<DegreeDto> getAllDegrees() {
        return Degree.findAll().stream().map(Degree::getDto).toList();
    }

    @Operation(summary = "Get degree by id")
    @GetMapping("/api/v1/references/degrees/{id}")
    public ResponseEntity<DegreeDto> getDegreeById(@PathVariable("id") Integer id) {
        return ResponseEntity.of(Degree.findById(id).map(Degree::getDto));
    }

    @Operation(summary = "Get all priorities")
    @GetMapping("/api/v1/references/priorities")
    public List<PriorityDto> getAllPriorities() {
        return Priority.findAll().stream().map(Priority::getDto).toList();
    }

    @Operation(summary = "Get priority by id")
    @GetMapping("/api/v1/references/priorities/{id}")
    public ResponseEntity<PriorityDto> getPriorityById(@PathVariable("id") Integer id) {
        return ResponseEntity.of(Priority.findById(id).map(Priority::getDto));
    }
}
