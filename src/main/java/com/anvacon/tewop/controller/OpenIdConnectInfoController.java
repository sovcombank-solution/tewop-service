package com.anvacon.tewop.controller;

import com.anvacon.tewop.advice.SecurityControllerAdvice;
import com.anvacon.tewop.dto.sso.OpenIDConnectUser;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import static com.anvacon.tewop.Names.SECURITY_SCHEME;

@Tag(name = "OpenId Connect Info")
@RequiredArgsConstructor
@RestController
public class OpenIdConnectInfoController {

    @Operation(summary = "Get OIDC user info from JwtAuthenticationToken for authenticated user")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Ok", content =
            {@Content(mediaType = "application/json", schema =
            @Schema(implementation = OpenIDConnectUser.class))}),
        @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
        @ApiResponse(responseCode = "404", description = "OIDC authenticated user not found", content = @Content)})
    @SecurityRequirement(name = SECURITY_SCHEME)
    @GetMapping("/api/v1/authenticated-user/oidc-user-info")
    public ResponseEntity<OpenIDConnectUser> getOidcUserInfoByJwtAuthenticationToken(
        @Parameter(hidden = true) @ModelAttribute(SecurityControllerAdvice.OIDC_USER_INFO)
        Optional<OpenIDConnectUser> user
    ) {
        return ResponseEntity.of(user);
    }
}
