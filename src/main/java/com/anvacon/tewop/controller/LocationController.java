package com.anvacon.tewop.controller;

import com.anvacon.tewop.dto.LocationDto;
import com.anvacon.tewop.service.LocationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.anvacon.tewop.Names.SECURITY_SCHEME;
import static org.springframework.data.domain.Sort.Direction.ASC;

@Tag(name = "Locations", description = "Локации агентской сети")
@RequiredArgsConstructor
@RestController
@SecurityRequirement(name = SECURITY_SCHEME)
public class LocationController {

    private final LocationService locationService;

    @Operation(summary = "Get all locations")
    @Parameter(in = ParameterIn.QUERY,
        description = "Sorting criteria in the format: property(,asc|desc). " +
            "Default sort order is ascending. " +
            "Multiple sort criteria are supported.",
        name = "sort",
        required = false,
        array = @ArraySchema(schema = @Schema(type = "Long", defaultValue = "externalId,asc")))
    @GetMapping("/api/v1/locations")
    public ResponseEntity<List<LocationDto>> getAllLocations(
        @Parameter(hidden = true) @SortDefault(sort = "externalId", direction = ASC) Sort sort
    ) {
        return ResponseEntity.ok(locationService.findAll(sort));
    }

    @Operation(summary = "Get location by id")
    @GetMapping("/api/v1/locations/{id}")
    public ResponseEntity<LocationDto> getLocationById(@PathVariable Long id) {
        return ResponseEntity.of(locationService.findById(id));
    }

    @Operation(summary = "Update location and create if not exists")
    @PostMapping("api/v1/locations")
    public ResponseEntity<LocationDto> mergeLocation(@Valid @RequestBody LocationDto dto) {
        return ResponseEntity.ok(locationService.save(dto));
    }

    @Operation(summary = "Delete location by id")
    @DeleteMapping("api/v1/locations/{id}")
    public void deleteById(@PathVariable Long id) {
        locationService.deleteById(id);
    }

    @Operation(summary = "Delete locations by ids")
    @DeleteMapping("api/v1/locations")
    public void deleteById(@Valid @RequestBody List<Long> ids) {
        locationService.deleteAllById(ids);
    }
}
